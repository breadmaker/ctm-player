# CTM Player

[![build status](https://gitlab.com/breadmaker/ctm-player/badges/master/pipeline.svg)](https://gitlab.com/breadmaker/ctm-player/commits/master)

## What is this?

[`CTMPlayer`](https://breadmaker.gitlab.io/ctm-player/) is a full, responsive UI and extended playback support for HTML5 [`<audio>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio) built to work with the [Ogg container format](https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Containers#Ogg).
It also support single files and multiple sources (Defined as a separate list of mutually exclusive sources, and **not** as a fallback list as currently defined by the standard).

## Usage

Just add `data-extend="ctm-player"` to the `<audio>` element and voila! Or initialize that element via JavaScript, where you can fine-tune aspect and behavior.

>  :information_source: Although the type of audio to be played is automatically detected, for a consistent behavior across browsers, we recommend adding `preload="metadata"` to single files and `preload="none"` to streams.

### Basic

Just add the `data-extend="ctm-player"` attribute to any `<audio>` element and you're ready to go. Optionally, you can also add `data-className="class1 class2"` to customize how the player container looks.

HTML:

```html
<audio data-extend="ctm-player" src="http://domain.com/audio.ogg" preload="none"></audio>
```

### Advanced

Initialize via JavaScript, and you will be able to define a custom template, custom container classes, a source list, and extend player functionalities with custom events listeners.

>  :information_source: You can create listeners to [all the events defined by the current standard](https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Media_events).

HTML:

```html
<audio id="my-selector" src="http://domain.com/audio.ogg" preload="none"></audio>
```

JavaScript:

```javascript
var player = new CTMPlayer({
    selector: "my-selector",              // <audio> element id attribute
    template: "template-name",            // Template id attribute
    className: "class1 class2",           // Container optional classes (separated by spaces)
    src: "http://domain.com/audio.ogg",   // Starting source
    sources: [{                           // The audio sources list
        name: "Low",                      // Sources are showed in the same order as defined
        url: "http://domain.com/audio_low.ogg",
    }, {
        name: "Normal",
        url: "http://domain.com/audio.ogg",
    }, {
        name: "High",
        url: "http://domain.com/audio_high.ogg"
    }],
    source_message: "Select quality",     // Source selector title
    events: {                             // Custom event triggers, those are assigned
        play: function() {                // BEFORE the ones defined by CTMPlayer
           console.log("play");
        },
        volumechange: function() {
           console.log("volumechange");
        }
    }
});
```

### Custom templates

You can define your own custom template for the player, adding a ```<script>```
tag of type ```text/x-tmpl``` **before** adding CTMPlayer's JavaScript file:

HTML:

```html
<audio id="custom" src="http://domain.com/audio.ogg" preload="none"></audio>
```

Plantilla:

```plain
<script id="custom-template" type="text/x-tmpl">
    <div class="ctm-player-html5-player">
      {%#o.audio%}
    </div>
    <button class="ctm-player-play">
        <i class="icon-play"></i>
    </button>
    <button class="ctm-player-stop ctm-player-hidden">
        <i class="icon-stop"></i>
    </button>
    <input type="range" max="1" step="0.01" value="1"
        oninput="custom.audio.volume=this.value;">
</script>
```

JavaScript:

```javascript
var custom = new CTMPlayer({
    selector: "custom",
    className: "",
    template: "custom-template"
});
console.log(custom);
```

## How can I test it?

You can see it in action on the [project website](https://breadmaker.gitlab.io/ctm-player/).
